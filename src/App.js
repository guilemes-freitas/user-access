import { Switch, Route } from "react-router";
import "./App.css";
import { members } from "./members.js";
import Customer from "./pages/customer";
import Company from "./pages/company";
import Links from "./components/Links";
import AddMember from "./components/AddMember";
import { useState } from "react";

function App() {
  const [membersArray, setMembersArray] = useState(members);

  const HandleMembers = (name, type) => {
    const id = (membersArray.length + 1).toString();
    const member = {
      id: id,
      name: name,
      type: type,
    };
    setMembersArray([...membersArray, member]);
  };
  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route path="/customer/:id">
            <Customer members={membersArray}></Customer>
          </Route>
          <Route path="/company/:id">
            <Company members={membersArray}></Company>
          </Route>
          <Route exact path="/">
            <Links members={membersArray} />
            <AddMember HandleMembers={HandleMembers} />
          </Route>
        </Switch>
      </header>
    </div>
  );
}

export default App;
