import { Link, useParams } from "react-router-dom";

const Customer = (props) => {
  const params = useParams();
  const member = props.members.find((member) => {
    return member.id === params.id;
  });
  console.log(member);

  return (
    <div>
      <h1>Detalhes do cliente</h1>

      <p>Nome: {member && member.name}</p>

      <Link to="/">Voltar</Link>
    </div>
  );
};

export default Customer;
