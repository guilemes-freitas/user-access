import { Link, useParams } from "react-router-dom";

const Company = (props) => {
  const params = useParams();
  const member = props.members.find((member) => {
    return member.id === params.id;
  });
  console.log(member);
  return (
    <div>
      <h1>Detalhes da Empresa</h1>

      <p>Nome da empresa: {member && member.name}</p>

      <Link to="/">Voltar</Link>
    </div>
  );
};

export default Company;
