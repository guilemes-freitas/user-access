import { useState } from "react";

const AddMember = (props) => {
  const [type, setType] = useState("pf");
  const [name, setName] = useState("");
  return (
    <section>
      <input
        placeholder="Nome"
        onChange={(e) => {
          setName(e.target.value);
        }}
      ></input>
      <select
        onChange={(e) => {
          setType(e.target.value);
        }}
      >
        <option value="pf">PF</option>
        <option value="pj">PJ</option>
      </select>
      <button
        onClick={() => {
          props.HandleMembers(name, type);
        }}
      >
        Adicionar
      </button>
    </section>
  );
};

export default AddMember;
