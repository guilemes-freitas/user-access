import React from "react";
import { Link } from "react-router-dom";

const Links = (props) => {
  return (
    <>
      {props.members.map((member) => {
        return member.type === "pf" ? (
          <Link to={`/customer/${member.id}`}>{member.name}</Link>
        ) : (
          <Link to={`/company/${member.id}`}>{member.name}</Link>
        );
      })}
    </>
  );
};

export default Links;
